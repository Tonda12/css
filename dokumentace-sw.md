# Možnosti

# 1)
Po zmáčknutí tlačítka s hodnocením se probudí ESP z deep sleep módu,
zaregistruje které tlačítko bylo zmáčknuto a odešle zprávu na MQTT broker.
Tlačítko bude potřeba držet dokud se ESPčko neprobudí a nezaregistruje, které tlačítko bylo zmáčknuto.
Toho může být docíleno kotrolkou, která se rozsvítí, až bude možno tlačítko pustit.
(Záleží na tom za jak dlouho se ESP nastartuje a jak je dlouhá průměrná doba stisku)

# 2)
ESP bude mít vypnutý modem a bude běžet ve smyčce, kde se každý interval probudí (z light sleepu)
a zkontroluje zda není některé z tlačítek stisknuté, v případě že je se zapne modem a odešle se zpráva do MQTT brokeru.
je možné prodloužit interval spánku, pokud bude u tlačítek zapojený nějaký obvod s kondenzátorem, který prodlouží signál.
Toto řešení nešetří moc spotřebu elektřiny, takže může nastat problém při provozu na baterky.

# 3)
Signál ze zmáčknutí tlačítka bude prodloužen na dostatečně dlouho,
takže ho bude moct zaznamenat ESP, když se probudí z deep sleep módu a bude moct odeslat zprávu MQTT brokeru.
Bude ale potřeba sestavit obvod s kondenzátory (a možná i s několika tranzistory a diodami),
aby signál mohl být prodloužen.